﻿using System;

namespace MergeSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 9, 2, 6, 5 };
            PrintArray(array);
            Console.WriteLine("***************");
            MergeSort(array);
        }

        static int[] MergeSort(int[] array)
        {
            if (array.Length == 1)
            {
                Console.WriteLine(array[0]);
            }
            else
            {
                int[] sub1 = new int[array.Length / 2];
                int[] sub2 = new int[array.Length - array.Length / 2];
                SubdivideArrays(array, sub1, sub2);
                MergeSort(sub1);
                MergeSort(sub2);
            }
            return null;
        }

        static void SubdivideArrays(int[] original, int[] sub1, int[] sub2)
        {
            if(original.Length<2)
            {
                return;
            }
            int middleIndex = original.Length / 2;
            Array.Copy(original, 0, sub1, 0, middleIndex);
            Array.Copy(original, middleIndex, sub2, 0, original.Length - middleIndex);
        }

        static void PrintArray(int[] array)
        {
            foreach (int elem in array)
            {
                Console.WriteLine(elem);
            }
        }
    }
}
